package getfifteen;
import java.awt.*;

@SuppressWarnings("serial")
public class Can extends Canvas
{
	private Image myImage;
    
    public Can(Image image)
    {
        myImage = image;
    }
    
    public void paint(Graphics g)
    {
        g.drawImage(myImage, 0, 0, this);
    }
    
    public void setImage(Image data)
    {
      myImage = data;
      this.repaint();
    }
}