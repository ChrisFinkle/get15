package getfifteen;

public class Get15
{
    // Properties
    private Controller myController;
    
    // Methods
    public static void main(String[] args)
    {
        new Get15();
    }
    
    public Get15()
    {
        setController(new Controller());
    }

	public void setController(Controller controller) 
	{
		myController = controller;
	}

	public Controller getController() 
	{
		return myController;
	}
}