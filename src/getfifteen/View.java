package getfifteen;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.lang.reflect.Method;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

@SuppressWarnings("serial")
public class View extends Frame
{
	private Controller myController;
	private Can[][] mySquares;
	private ButtonListener[][] mySquareListener;
	private JLayeredPane myLP;
	private Image myBgImage;
	private ImageIcon myBgIcon;
	private JLabel myBackground;
	private JButton myResetButton;
	private ButtonListener myResetListener;
	
	public View(Controller controller)
	{
		this.setSize(600,700);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        myBgImage = Toolkit.getDefaultToolkit().getImage("images/Background.png");
        myBgIcon = new ImageIcon(myBgImage);
        myBackground = new JLabel(myBgIcon);
    	myBackground.setBounds(0,0,600,700);
    	myResetButton = new JButton("Reset");
    	myResetButton.setBounds(260,600,80,30);
    	
		myController = controller;
		mySquares = new Can[5][5];
		mySquareListener = new ButtonListener[5][5];
		myLP = new JLayeredPane();
		myLP.setPreferredSize(new Dimension(600,700));
		myLP.add(myBackground, new Integer(0));
		myLP.add(myResetButton, new Integer(6));
		for(int i=0; i<5; i++)
		{
			for(int j=0; j<5; j++)
			{
				mySquares[i][j] = new Can(myController.getSquareImage(i,j));
				mySquares[i][j].setSize(100,110);
				mySquares[i][j].setBounds(50+100*i,50+100*j,100,110);
				myLP.add(mySquares[i][j], new Integer(j+1));
			}
		}	
		this.add(myLP);
		this.setVisible(true);
        this.addWindowListener(new AWindowListener());
        this.associateListeners();
	}
	
	public void associateListeners()
    {
        Class<? extends Controller> controllerClass;
        Method tapMethod, resetMethod;
        Class<?>[] classArgs;

        controllerClass = myController.getClass();
        
        tapMethod = null;
        resetMethod = null;
        classArgs = new Class[2];
        
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
           classArgs[1] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
        }
        
        try
        {
          tapMethod = controllerClass.getMethod("tap",classArgs);
          resetMethod = controllerClass.getMethod("reset");
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
        }
        
        int i;
        Integer[] args;

        for (i=0; i < 5; i++)
        {
        	for(int j=0; j<5; j++)
        	{
               args = new Integer[2];
	           args[0] = new Integer(i);
	           args[1] = new Integer(j);
	           mySquareListener[i][j] = new ButtonListener(myController, tapMethod, args);
	           mySquares[i][j].addMouseListener(mySquareListener[i][j]);
        	}
        }
        
        myResetListener = new ButtonListener(myController, resetMethod, null);
        myResetButton.addMouseListener(myResetListener);
    }
	
	public void updateImages()
	{
		for(int i=0; i<5; i++)
		{
			for(int j=0; j<5; j++)
			{
				mySquares[i][j].setImage(myController.getSquareImage(i,j));
				if(myController.getSquareTapped(i,j))
				{
					mySquares[i][j].setBounds(50+100*i,40+100*j,100,110);
				}
				else
				{
					mySquares[i][j].setBounds(50+100*i,50+100*j,100,110);
				}
			}
		}
	}

}
