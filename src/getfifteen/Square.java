package getfifteen;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Vector;

public class Square {
	private int myX;
	private int myY;
	private int myNum;
	private Image myImage;
	private boolean myTapped;
	private Model myModel;
	private Vector<Square> myAdjacent;
	
	public Square(Model m, int x, int y)
	{
		myX = x;
		myY = y;
		myModel = m;
		myTapped = false;
		this.updateImage();
	}
	
	public Vector<Square> checkAdjacent()
	{
		Vector<Square> adj = new Vector<Square>(0);
		int dim = myModel.getDimension();
		if(myX>0)
		{
			Square s = myModel.getSquare(myX-1, myY);
			if(s.getNum() == myNum && !s.tapped())
			{
				adj.add(s);
			}
		}
		if(myX<dim-1)
		{
			Square s = myModel.getSquare(myX+1, myY);
			if(s.getNum() == myNum && !s.tapped())
			{
				adj.add(s);
			}
		}
		if(myY>0)
		{
			Square s = myModel.getSquare(myX, myY-1);
			if(s.getNum() == myNum && !s.tapped())
			{
				adj.add(s);
			}
		}
		if(myY<dim-1)
		{
			Square s = myModel.getSquare(myX, myY+1);
			if(s.getNum() == myNum && !s.tapped())
			{
				adj.add(s);
			}
		}
		myAdjacent = adj;
		return adj;
	}
	
	public void shiftDown()
	{
		if(myY>0)
		{
			Square myNorth = myModel.getSquare(myX, myY-1);
			myNum = myNorth.getNum();
			myTapped = myNorth.tapped();
		}
		else if(myY==0)
		{
			myNum = myModel.addRandom();
			myTapped = false;
		}
		this.updateImage();
	}
	
	public void updateImage()
	{
		myImage = Toolkit.getDefaultToolkit().getImage("images/"+this.toString()+".png");
	}
	
	public Vector<Square> getAdjacent()
	{
		return myAdjacent;
	}
	
	public void tap()
	{
		myTapped = true;
		this.updateImage();
	}
	
	public void untap()
	{
		myTapped = false;
		this.updateImage();
	}
	
	public boolean tapped()
	{
		return myTapped;
	}
	
	public void incrementNum()
	{
		myNum++;
		this.updateImage();
		Model m = myModel;
		if(myNum>m.getHighestTile())
		{
			m.incrementHighest();
		}
	}
	public void setNum(int i)
	{
		myNum = i;
	}
	
	public int getNum()
	{
		return myNum;
	}
	
	public String toString()
	{
		return String.valueOf(myNum) + (myTapped ? "t":"");
	}
	
	public int getY()
	{
		return myY;
	}
	
	public int getX()
	{
		return myX;
	}
	
	public Image getImage()
	{
		this.updateImage();
		return myImage;
	}
}
