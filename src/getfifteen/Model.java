package getfifteen;

import java.util.Random;
import java.util.Vector;

public class Model 
{
	private Square[][] myContents;
	private int myDimension;
	private int myHighestTile;
	private boolean myLost = false;
	private Random myRn;
	private boolean myTapped = false;
	
	/**
	 * Constructs and initializes model
	 * @param dim square dimension of model
	 * @author Christopher Finkle
	 */
	public Model(int dim)
	{
		myRn = new Random();
		myHighestTile = 5;
		myDimension=dim;
		myContents = new Square[dim][dim];
		for(int i=0; i<dim; i++)
		{
			for(int j=0; j<dim; j++)
			{
				Square s = new Square(this,i,j);
				s.setNum(this.addRandom());
				myContents[i][j] = s;
			}
		}
		for(int i=0; i<dim; i++)
		{
			for(int j=0; j<dim; j++)
			{
				myContents[i][j].checkAdjacent();
			}
		}
	}
	
	public Square[][] getContents()
	{
		return myContents;
	}
	
	/**
	 * Gets square at coordinates x,y
	 * 
	 * @author Christopher Finkle
	 */
	public Square getSquare(int x, int y)
	{
		return myContents[x][y];
	}
	
	public int getDimension()
	{
		return myDimension;
	}
	
	/**
	 * Gives random int of appropriate value for current game state
	 * @return random int
	 * 
	 * @author Christopher Finkle
	 */
	public int addRandom()
	{
		int cap = (myHighestTile>8 ? 7 : myHighestTile-1);
		int i = myRn.nextInt(cap);
		if(i==0)
		{
			i=1;
		}
		return i;
	}
	
	/**
	 * If square at x,y is tapped, collapses selection
	 * If not tapped, taps if valid
	 * @return whether tapped or not
	 * @author Christopher Finkle
	 */
	public boolean tap(int x, int y)
	{
		Square t = myContents[x][y];
		if(t.tapped())
		{
			this.collapse(x,y);
			myTapped = false;
			return true;
		}
		for(int i = 0; i<myDimension; i++)
		{
			for(int j=0; j<myDimension; j++)
			{
				myContents[i][j].untap();
				this.updateAdjacents();
			}
		}
		Vector<Square> tapped = t.getAdjacent();
		if(tapped.isEmpty())
		{
			return false;
		}
		t.tap();
		int s = tapped.size();
		while(!tapped.isEmpty())
		{
			for(int i=0; i<s; i++)
			{
				t = tapped.get(i);
				t.tap();
				tapped.addAll(t.checkAdjacent());
			}
			s = tapped.size();
			for(int j = 0; j<s; j++)
			{
				if(tapped.get(j).tapped())
				{
					tapped.remove(j);
					s--;
				}
			}
		}
		myTapped = true;
		return true;
	}
	
	/**
	 * 'Collapses' current selection into x,y, then 'removes' vanished squares
	 * @param x
	 * @param y
	 * @author Christopher Finkle
	 */
	public void collapse(int x, int y)
	{
		Square t = myContents[x][y];
		t.incrementNum();
		t.untap();
		this.removeTapped();
		this.updateAdjacents();
	}
	
	/**
	 * Goes through each column removing tapped squares
	 * and replacing them with randoms at the top
	 * 
	 * @author Christopher Finkle
	 */
	public void removeTapped()
	{
		for(int i=0; i<myDimension; i++)
		{
			Square[] col = myContents[i];
			for(int j=0; j<myDimension; j++)
			{
				if(col[j].tapped())
				{
					for(int k=j; k>-1; k--)
					{
						col[k].shiftDown();
					}
				}
			}
		}
	}
	
	/**
	 * Makes every square check which squares it is adjacent to
	 * @author Christopher Finkle
	 */
	public void updateAdjacents()
	{
		for(int j=0; j<myDimension; j++)
		{
			for(int i=0; i<myDimension; i++)
			{
				myContents[i][j].checkAdjacent();
			}
		}
	}
	
	/**
	 * Checks whether any moves are left
	 * @return
	 */
	public boolean noMoves()
	{
		this.updateAdjacents();
		boolean noMoves = true;
		for(int j=0; j<myDimension; j++)
		{
			for(int i=0; i<myDimension; i++)
			{
				if(!myContents[i][j].getAdjacent().isEmpty())
				{
					noMoves = false;
				}
			}
		}
		if(myTapped)
		{
			noMoves = false;
		}
		myLost = noMoves;
		return myLost;
	}
	
	
	public int getHighestTile()
	{
		return myHighestTile;
	}
	
	public void incrementHighest()
	{
		myHighestTile++;
	}
}
