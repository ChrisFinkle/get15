package getfifteen;

import java.awt.Image;

import javax.swing.JOptionPane;

public class Controller {
	private Model myModel;
	private View myView;
	
	public Controller()
	{
		myModel = new Model(5);
		myView = new View(this);
	}
	
	public void tap(Integer x, Integer y)
	{
		myModel.tap(x,y);
		myView.updateImages();
		if(myModel.noMoves())
		{
			 String s = "You lost. Play again?";
			   
			   if (JOptionPane.showConfirmDialog(myView, s, "NO MOVES!",
				        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
			   {
				   this.reset();
			   }
			   else
			   {
				   System.exit(0);
			   }
		}
	}
	
	public void reset()
	{
		myModel = new Model(5);
		myView.dispose();
		myView = new View(this);
	}
	
	public Image getSquareImage(Integer x, Integer y)
	{
		return myModel.getSquare(x,y).getImage();
	}
	
	public boolean getSquareTapped(int x, int y)
	{
		return myModel.getSquare(x, y).tapped();
	}

}
